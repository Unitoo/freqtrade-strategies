# Freqtrade strategie

Questo repository contiene tutte le strategie che vogliamo valutare o riteniamo efficaci. 
Ogni configurazione deve risiedere in una cartella con il nome della moneta su cui fare stacking (ES: USDT).

Il nome delle configurazione devono identificare a grandi linee su cosa si basa la strategia, tipo:
- LowBudget.yml
- DailyProfit.yml

Un README interno potrebbe riassumere i parametri significativi della configurazione e lo scopo.
