### VET/USDT

| File | Start date | End date | Tot. Profit | Avg. Profit | Objective |
| ---- | ---------- | -------- | ----------- | ----------- | --------- |
| [VET_1](USDT/VET_1.py) | 2021-02-15 06:00:00 | 2021-04-26 09:55:00 | 230.33% | 1.18% | -14.7 |
